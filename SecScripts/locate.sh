#!/bin/bash
sl (){
    sleep 1
}

tput setaf 6;printf "\n";date;printf "\n";echo "Currently at..."
tput setaf 5;echo "Public IP:";sl
tput setaf 2;wget -O - -q icanhazip.com;sl
tput setaf 5;echo "Location:";sl
tput setaf 2;curl ipinfo.io/loc;sl
tput setaf 5;echo "_gateway:";sl
tput setaf 2;ip route get 8.8.8.8 | sed -n '/src/{s/.*src *\([^ ]*\).*/\1/p;q}';sl
# ip -4 -o addr show >/dev/null | awk '{print $4}'
tput setaf 5;echo "Interfaces:";sl
tput setaf 4;netstat -i; printf "\n";sl

