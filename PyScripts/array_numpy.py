#!/usr/bin/python3

"""
Arrays on Numpy
"""
import numpy as np
my_list = [1,2,3]
my_array = np.array(my_list)
print (my_array)
print (my_array.shape)

"""
Matriz numpy
"""
my_list2 = [[1,2,3] , [4,5,6]]
my_array2 = np.array(my_list2)
print (my_array2)
print (my_array2.shape)
print ("Firts row:",my_array2[0])
print ("Last row:",my_array2[-1])
print ("First row first value:",my_array2[0,0])
print ('Last row last value:',my_array2[-1,-1])
print ('Last value from both rows:',my_array2[ : , -1])

"""
Vectorization on Numpy
"""
my_array3 = np.array([3,3,3])
my_array4 = np.array([5,5,5])
print ('Sum of both rows first values:', my_array3[0], "+", my_array4 [0], "=", my_array3[0] + my_array4[0])
